/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polinomio;

import com.sun.org.apache.xpath.internal.VariableStack;
import java.io.InputStreamReader;
import java.rmi.Naming;
import java.util.Scanner;

/**
 *
 * @author charlie
 */
public class Cliente {
    private static char[] variables = {'x', 'y', 'z'};
  
    public static int[] LeerPolinomio(String message) {
        int[] Polinomio = new int[3];
        Scanner sc = new Scanner(System.in);
        System.out.println(message);
        for( int i = 0; i < 3; i++) {
            System.out.print(variables[i] + ": ");
            Polinomio[i] = sc.nextInt();
        }
        return Polinomio;
    }
    
    public static void MostrarPolinomio(int[] polinomio) {
        for( int i = 0; i < 3; i++){
            System.out.println( variables[i] + ": " + polinomio[i]);
        }
    }
    
    public static void LeerSeleccion() {
        try {
            int seleccion;
            int[] polinomioA;
            int[] polinomioB;
            int[] resultado;
            Interface i = (Interface)Naming.lookup("rmi://192.168.1.104/Polinomio");
            System.out.print("Digite su Opcion: ");
            Scanner sc = new Scanner(new InputStreamReader(System.in));
            seleccion = sc.nextInt();
            
            switch(seleccion) {
                case 1:
                    System.out.println("\n\nSumar: ");
                    polinomioA = LeerPolinomio("=> Primer Polinomio");
                    polinomioB = LeerPolinomio("=> Segundo Polinomio");
                    resultado = i.Sumar(polinomioA, polinomioB);
                    System.out.println("===>> Resuktado: ");
                    MostrarPolinomio(resultado);
                    Menu();
                    break;
                case 2:
                    System.out.println("\n\nRestar: ");
                    polinomioA = LeerPolinomio("==> Primer Polinomio");
                    polinomioB = LeerPolinomio("==> Segundo Polinomio");
                    resultado = i.Restar(polinomioA, polinomioB);
                    System.out.println("===>>Resultado: ");
                    MostrarPolinomio(resultado);
                    Menu();
                    break;
                case 3:
                    System.out.println("\n\nMultiplicar: ");
                    polinomioA = LeerPolinomio("==> Primer Polinomio");
                    polinomioB = LeerPolinomio("==> Segundo Polinomio");
                    resultado = i.Multiplicar(polinomioA, polinomioB);
                    System.out.println("===>> Resuktado: ");
                    MostrarPolinomio(resultado);
                    Menu();
                    break;
                case 4:
                    System.exit(0);
                default:
                    System.out.print("Opcion Incorrecta");
                    Menu();
                    break;
            }
        } catch (Exception e) {
        }
    }
    
    public static void Menu() {
        System.out.println("\n\n------------------");
        System.out.println("--     Menu     --");
        System.out.println("------------------");
        System.out.println("1. Sumar");
        System.out.println("2. Restar");
        System.out.println("3. Multiplicar");
        System.out.println("4. Salir");
        LeerSeleccion();
    }
    
    /**
     * Main
     * @param args 
     */
    public static void main(String[] args) {
        try {
            Menu();
        } catch (Exception e) {
        }
    }
}

